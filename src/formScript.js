fetch("./formExampls.json")
  .then((res) => res.json())
  .then((json) => {
    console.log(json);
    for (const [key, value] of Object.entries(json.forms)) {
      createFormButton(value);
    }
    // for (const [key, value] of Object.entries(json.forms)) {
    //   createForm(value);
    // }
  });

function createFormButton(formData) {
  let button = document.createElement("button");
  button.setAttribute("href", "#");
  button.setAttribute("class", "button");
  //   button.setAttribute("id", formData.id);
  button.appendChild(document.createTextNode(formData.nom));
  document.body.appendChild(button);
  createPopup(formData);
  button.addEventListener("click", () => {
    openForm(formData.id);
  });
  document.body.appendChild(document.createElement("br"));
}

function createPopup(formData, isAdd = false) {
  console.log(formData.id);
  let container = document.createElement("div");
  container.setAttribute("class", "formPopup");
  container.setAttribute("id", formData.id);
  let form = document.createElement("form");
  form.setAttribute("id", formData.id + "f");
  formData.inputs.map((inputData) => {
    let label = document.createElement("label");
    label.appendChild(document.createTextNode(inputData.title));
    label.setAttribute("class", "titleLabel");
    let input = document.createElement("input");
    input.setAttribute("type", inputData.type);
    input.setAttribute("placeholder", inputData.placeHolder);
    input.setAttribute("id", inputData.id);
    input.setAttribute("class", "textInput");
    form.appendChild(label);
    form.appendChild(document.createElement("br"));
    form.appendChild(input);
    form.appendChild(document.createElement("br"));
  });
  let buttonContainer = document.createElement("div");
  buttonContainer.setAttribute("class", "buttonContainer");
  let close = document.createElement("button");
  close.setAttribute("type", "button");
  close.setAttribute("class", "button");
  close.setAttribute("href", "#");
  close.appendChild(document.createTextNode("Close"));
  close.addEventListener("click", () => {
    closeForm(formData.id);
  });

  let add = document.createElement("button");
  add.appendChild(document.createTextNode("Add"));
  add.setAttribute("href", "#");
  add.setAttribute("type", "button");
  add.setAttribute("class", "button");
  add.addEventListener("click", () => {
    // addInput("test", "text", "this is placeholder", formData.id + "f")
    if (!isAdd) {
      const addData = {
        id: formData.id + "fa",
        inputs: [
          {
            id: formData.id + "in1a",
            title: "Input title",
            type: "text",
            placeHolder: "Entrez l'input title",
          },
          {
            id: formData.id + "in2a",
            title: "Type d'input",
            type: "text",
            placeHolder: "Entrez le type d'input",
          },
          {
            id: formData.id + "in3a",
            title: "Placeholder",
            type: "text",
            placeHolder: "Entrez le placeHolder",
          },
        ],
      };
      createPopup(addData, true);
      openForm(formData.id + "fa");
    } else {
      let title = document.getElementById(
        formData.id.slice(0, -2) + "in1a"
      ).value;
      let type = document.getElementById(
        formData.id.slice(0, -2) + "in2a"
      ).value;
      let placeholder = document.getElementById(
        formData.id.slice(0, -2) + "in3a"
      ).value;
      addInput(title, type, placeholder, formData.id.slice(0, -2) + "f");
      closeForm(formData.id);
    }
  });
  buttonContainer.appendChild(add);
  buttonContainer.appendChild(close);
  container.appendChild(form);
  form.appendChild(buttonContainer);
  document.body.appendChild(container);
}

function openForm(id) {
  document.getElementById(id).style.display = "block";
}
function closeForm(id) {
  document.getElementById(id).style.display = "none";
}

function addInput(title, type, placeHolder, formId) {
  let label = document.createElement("label");
  label.appendChild(document.createTextNode(title));
  label.setAttribute("class", "titleLabel");
  let input = document.createElement("input");
  input.setAttribute("type", type);
  input.setAttribute("placeholder", placeHolder);
  input.setAttribute("class", "textInput");
  let form = document.getElementById(formId);
  form.insertBefore(label, form.lastChild);
  form.insertBefore(document.createElement("br"), form.lastChild);
  form.insertBefore(input, form.lastChild);
  form.insertBefore(document.createElement("br"), form.lastChild);
}

function addClickHandler() {
  let form = document.createElement("form");
}
