const loginForm = document.getElementById("login-form");
const loginButton = document.getElementById("login-button");
const loginError = document.getElementById("login-error-message-container");

loginButton.addEventListener("click", (e) => {
  e.preventDefault();
  const identifiant = loginForm.identifiant.value;
  const password = loginForm.password.value;
  if (identifiant === "admin" && password === "admin") {
    window.location.assign("form.html");
  } else {
    loginError.style.display = "block";
  }
});
