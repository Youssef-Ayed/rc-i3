# Assignment

## Setting up a development environment
You will need a system and git. <br>
I suggest using Ubuntu. <br>
While other systems also work, I cannot provide the set of packages you will need right now.

## Tasks

1. Read the test.
2. Run [rc-i3.sh](rc-i3.sh)
3. Implement and test.
4. Create a merge request with your changes on Gitlab.

Feel free to ask any questions.
